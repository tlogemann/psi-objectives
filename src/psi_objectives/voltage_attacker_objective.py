import logging
from typing import Dict, Optional

from palaestrai.agent.memory import Memory
from palaestrai.agent.objective import Objective
from .gauss import normal_distribution_pdf

LOG = logging.getLogger("palaestrai.agent.objective")


class VoltageBandViolationPendulum(Objective):
    VM_PU_LOW = 0.85
    VM_PU_HIGH = 1.15
    SIGMA = -0.05
    C = -10.0
    A = -12.0

    def __init__(self, params: Optional[Dict] = None):
        params = {} if not params else params
        super().__init__(params)
        self._sigma = params.get("sigma", VoltageBandViolationPendulum.SIGMA)
        self._c = params.get("c", VoltageBandViolationPendulum.C)
        self._a = params.get("a", VoltageBandViolationPendulum.A)
        self.step = 0

    def internal_reward(self, memory: Memory, **kwargs) -> float:
        """Expect Voltage values and reward voltage band violations."""
        vm_pu = float(
            memory.get_rewards()[-1:].filter(
                like="vm_pu-median", axis=1
            ).iloc[-1]
        )

        objective = normal_distribution_pdf(
            x=vm_pu,
            mu=1.0,
            sigma=self._sigma,
            c=self._c,
            a=self._a
        )
        objective += normal_distribution_pdf(
            x=vm_pu,
            mu=0.83,
            sigma=0.01,
            c=0.0,
            a=self._a
        )
        objective += normal_distribution_pdf(
            x=vm_pu,
            mu=1.16,
            sigma=0.01,
            c=0.0,
            a=self._a
        )

        self.step += 1
        return objective
