from typing import TYPE_CHECKING, List

from palaestrai.agent import Objective
import math

if TYPE_CHECKING:
    from palaestrai.agent import RewardInformation

class NeighborhoodyObjective(Objective):
    def __init__(self):
        self.battery_usages: list = []
        self.window: int = 20
        self.penalty: int = 0

    def internal_reward(self, rewards: List["RewardInformation"], **kwargs) -> float:

        renewable_energy = 0
        fossil_energy = 0
        ext_grid_active = 0
        ext_grid_reactive = 0
        for rew in rewards:
            if rew.reward_id == "renewable_energy":
                renewable_energy = rew.reward_value
            if rew.reward_id == "fossil_energy":
                fossil_energy = rew.reward_value
            if rew.reward_id == "ext_grid_active_usage":
                ext_grid_active = rew.reward_value
            if rew.reward_id == "ext_grid_reactive_usage":
                ext_grid_reactive = rew.reward_value
            if rew.reward_id == "storage_usage":
                self.battery_usages.append(rew.reward_value)

        value = composed_gauss(ext_grid_active, low=-0.25, high=0.25, scaling=100)

        storage_abs_sum: float = 0.0
        total = len(self.battery_usages)
        for idx in range(total, max(0, total-self.window-1), -1):
            storage_abs_sum += abs(self.battery_usages[idx-1]) 

        if storage_abs_sum == 0:
            self.penalty += 10
        elif self.penalty > 0:
            self.penalty = 0
        else:
            self.penalty -= 5

        value -= self.penalty
        print(f"{ext_grid_active:.4f}, {storage_abs_sum:.3f}, {value:.5f} {-self.penalty}")
        return value


def gauss_norm(
    raw_value: float,
    mu: float = 1,
    sigma: float = 0.1,
    c: float = 0.5,
    a: float = -1,
):
    if not isinstance(raw_value, float):
        try:
            raw_value = sum(raw_value)
        except TypeError:
            return 0
    gaus_reward = a * math.exp(-((raw_value - mu) ** 2) / (2 * sigma**2)) - c
    return gaus_reward


def composed_gauss(raw_value, low=-0.5, high=0.5, scaling=100):
    if raw_value < low:
        return raw_value * scaling * 0.5
    elif low <= raw_value <= high:
        return gauss_norm(raw_value, mu=0, sigma=0.1, a=scaling)
    else:
        return raw_value * scaling * -1 * 0.5